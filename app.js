//==========================>Import Mddules
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');


//==========================>Use Swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));



//==========================>Definde Routes
const createPage = require('./routes/create')
const readPage = require('./routes/read')
const updatePage = require('./routes/update')
const deletePage = require('./routes/delete')


//==========================>Import Database config
const datbase = require('./config/database')


//==========================>Use BodyParser
app.use(bodyParser.json());  // support parsing of application/json type post data
app.use(bodyParser.urlencoded({extended: false}));  //support parsing of application/x-www-form-urlencoded post data


//==========================>Use our routes
app.use(createPage)
app.use(readPage)
app.use(deletePage)
app.use(updatePage)

app.listen(3000)