const Person = require('../models/model.data');

//----------create controllers--------
module.exports.createPage = (req, res)=>{
    let person = new Person({         // inheritance from Person (Schema data)
        name : req.body.name,         //get data from bodyParser 
        family : req.body.family,
        city :req.body.city,
        age :req.body.age
    });

    person.save((err)=>{            //save function(mongoose's funnction) that will take a JSON object how get from bodyParser and store it in the database
        if(err){
            console.log(err);
        }
        else(
            res.send('Data sucssesfuly saved...')
        );
    });
};



//----------read controllers--------
module.exports.readPage = (req, res)=>{
    Person.findById(req.params.id, (err, person)=>{   //find a documnet with id and read data 
        if(err) console.log(err);
        res.send(person);
    })
};


//----------update controllers--------
module.exports.updatePage = (req, res)=>{
    Person.findByIdAndUpdate(req.params.id, {$set: req.body}, (err, person)=>{  //update a documnet with id and use $set to replace value ($set is MongoDB's operator )
        if(err) console.log(err);
        res.send('person updated...');
    });
};


//----------delete controllers--------
module.exports.deletePage = (req, res)=>{
    Person.findByIdAndRemove(req.params.id,(err)=>{  //delete a documnet with specific id 
        if(err) console.log(err);
        res.send('Deleted successfully');
    });
};