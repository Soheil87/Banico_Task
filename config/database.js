const mongoose = require('mongoose'); //add mongoose module 
const url = "mongodb://localhost:27017/Task"; 
const mongoDB = process.env.MONGODB_URI || url; 
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true}); //connect to MongoDB 
mongoose.set('useFindAndModify', false); 
mongoose.Promise = global.Promise;//global promise for use mongoose in all project 
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));