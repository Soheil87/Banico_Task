const mongoose = require('mongoose');  //import mongoose module
const Schema = mongoose.Schema;  //use schema for design database

let personData = new Schema({   //creat new object from schema & defines the shape of the documents 
    name   : {type:String, required:true, max:50},
    family : {type:String, required:true, max:50},
    city   : {type:String, required:true, max:50},
    age    : {type:String, required:true, max:75},
})


module.exports = mongoose.model('Persons', personData);