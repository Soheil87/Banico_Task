const express = require ('express');
const router  = express.Router();
const controllers = require('../controllers/controllers')

router.delete('/:id', controllers.deletePage) //delete method for delete data by id


module.exports = router;