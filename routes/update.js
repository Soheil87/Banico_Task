const express = require ('express');
const router  = express.Router();
const controllers = require('../controllers/controllers')

router.put('/:id', controllers.updatePage) //update data by put method


module.exports = router;