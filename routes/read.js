const express = require ('express');
const router  = express.Router();
const controllers = require('../controllers/controllers')

router.get('/:id', controllers.readPage) //read data by get method


module.exports = router;